# Bumper API

Bumper is a geo based dating application that helps people find connections they missed bumping into.

## Layer

The Bumper API is the first layer of the backend stack.
Designed to interact with mobile and web front ends, the API is responsible for receiving geo 
data, putting this data on a queue to be processed and retrieving information for a user.

## User Profiles

Each user can save a profile (up to 5 pictures; Job; Workplace; Short description) which is saved, 
and retrieved, through the API.

The user profile should initially be generated through the Facebook login.

## Location Data

Location data (lat, lon, datetime\_recorded) for a user, is uploaded, and saved, through the API.

## Data retrieval

Data retrieved:

* Bumps
* Likes
* History
  * 1/7/30 day history
  * Route
  * Total bumps
  * Bump heatmap


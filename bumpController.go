package bumperapi

import (
	bdb "bitbucket.org/kisamoto/bumperdb"
	"github.com/gin-gonic/gin"
)

type BumpController struct {
	db *bdb.BumperDB
}

func (b *BumpController) GET(c *gin.Context) {
	user := c.Params.ByName(userId)
	// Just echo back for now
	c.String(200, user)
}

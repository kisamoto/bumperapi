package bumperapi

import (
	bdb "bitbucket.org/kisamoto/bumperdb"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

type ReportController struct {
	db *bdb.BumperDB
}

func (m *ReportController) POST(c *gin.Context) {
	var errorType, errorDetail string
	var repRequest ReportRequest
	responseCode := http.StatusOK
	decoder := json.NewDecoder(c.Request.Body)
	userID := c.Params.ByName(userId)
	err := decoder.Decode(&repRequest)
	if err != nil {
		log.Println(err)
		errorType = errTypeInvalidJSON
		errorDetail = errDetailInvalidJSON
		responseCode = http.StatusBadRequest
	} else {
		log.Printf("I'm fine for: %v", userID)
	}
	c.JSON(responseCode, NewServerResponse(responseCode, errorType, errorDetail, nil))
}

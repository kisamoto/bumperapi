package main

import (
	bapi "bitbucket.org/kisamoto/bumperapi"
	"os"
)

func GetenvWithDefault(envVar, defaultValue string) (envVarValue string) {
	envVarValue = os.Getenv(envVar)
	if envVarValue == "" {
		envVarValue = defaultValue
	}
	return
}

func GetDBConfFromEnvVars() (redisProtocol, redisHost, redisLocQueue, redisMsgQueue,
	postgresURI, postgresUserSchema, postgresAnalyticsSchema string) {
	// Redis protocol
	redisProtocol = GetenvWithDefault("REDIS_PROTOCOL", "tcp")
	// Redis host
	redisHost = GetenvWithDefault("REDIS_HOST", ":6379")
	// Redis Location Queue
	redisLocQueue = GetenvWithDefault("REDIS_LOC_QUEUE", "locations")
	// Redis Message Queue
	redisMsgQueue = GetenvWithDefault("REDIS_MSG_QUEUE", "messages")

	// Postgres URI
	postgresURI = GetenvWithDefault("POSTGRES_URI", "postgres://bumper:bumper@localhost/bumper?sslmode=disable")
	// Postgres User Schema
	postgresUserSchema = GetenvWithDefault("POSTGRES_USER_SCHEMA", "usr")
	// Postgres Analytics Schema
	postgresAnalyticsSchema = GetenvWithDefault("POSTGRES_ANALYTICS_SCHEMA", "analytics")
	return
}

func GetHostConfFromEnvVars() (host string) {
	// Default to running http on localhost
	host = GetenvWithDefault("SERVER_HOST", ":8080")
	return
}

func main() {
	server := bapi.NewServer(GetDBConfFromEnvVars())
	server.Run(GetHostConfFromEnvVars())
}

package bumperapi

import (
	bdb "bitbucket.org/kisamoto/bumperdb"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

type MessageController struct {
	db *bdb.BumperDB
}

func (m *MessageController) POST(c *gin.Context) {
	var errorType, errorDetail string
	var msgRequest MessageRequest
	responseCode := http.StatusOK
	userID := c.Params.ByName(userId)
	decoder := json.NewDecoder(c.Request.Body)
	err := decoder.Decode(&msgRequest)
	if err != nil {
		log.Println(err)
		errorType = errTypeInvalidJSON
		errorDetail = errDetailInvalidJSON
		responseCode = http.StatusBadRequest
	} else {
		log.Printf("I'm fine for %v", userID)
	}
	c.JSON(responseCode, NewServerResponse(responseCode, errorType, errorDetail, nil))
}

func (m *MessageController) GET(c *gin.Context) {
	var errorType, errorDetail string
	statusCode := http.StatusTeapot
	user := c.Params.ByName(userId)
	// Send back "I'm a teapot" with a message to be displayed on device
	mes := ServerMessage{MessageType: "test_message", Message: user}
	res := NewServerResponse(statusCode, errorType, errorDetail, mes)
	c.JSON(statusCode, res)
}

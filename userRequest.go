package bumperapi

import (
	bdb "bitbucket.org/kisamoto/bumperdb"
)

/*
Example POST/PUT data

{
	user: {
		"id":
		"first_name":
		"last_name":
		"age": 30,
		"gender": m,
		"men": true,
		"women": false,
		"min_age": 22,
		"max_age": 30,
		"pics": []
	}
}
*/
type UserRequest struct {
	User bdb.UserProfile `json:"user"`
}

package bumperapi

type ResponseMeta struct {
	Code        int    `json:"code"`
	ErrorType   string `json:"error_type,omitempty"`
	ErrorDetail string `json:"error_detail,omitempty"`
}

func NewResponseMeta(resCode int, errorType, errorDetail string) ResponseMeta {
	return ResponseMeta{Code: resCode, ErrorType: errorType, ErrorDetail: errorDetail}
}

type ResultsResponse struct {
	Results interface{} `json:"results"`
}

func NewResponseResponse(result interface{}) ResultsResponse {
	return ResultsResponse{Results: result}
}

type ServerResponse struct {
	Meta     ResponseMeta `json:"meta"`
	Response interface{}  `json:"response"`
}

type ServerMessage struct {
	MessageType string `json:"message_type"`
	Message     string `json:"message"`
}

func NewServerResponse(resCode int, errorType, errorDetail string, result interface{}) ServerResponse {
	var r interface{}
	m := NewResponseMeta(resCode, errorType, errorDetail)
	// If any form of error detected, don't include any results
	if errorType == "" && errorDetail == "" {
		r = result
	}
	return ServerResponse{Meta: m, Response: r}
}

package bumperapi

// Versions
const (
	home     = "/"
	version1 = "v1"
)

// Endpoints
const (
	locGroup     = "/location"
	userGroup    = "/user"
	bumpGroup    = "/bump"
	likeGroup    = "/like"
	messageGroup = "/message"
	historyGroup = "/history"
	blockGroup   = "/block"
	reportGroup  = "/report"
)

// Variable extraction
const (
	userId = "user"
)

// Error type messages
const (
	errTypeInvalidParameter = "invalid_param"
	errTypeInvalidJSON      = "invalid_json"
	errTypeMissingParameter = "missing_param"
)

// Error detail messages
const (
	errDetailInvalidParameter = "Malformed URL requested"
	errDetailInvalidJSON      = "Unable to decode provided JSON"
	errDetailMissingParameter = "Missing GET parameter in this request"
)

package bumperapi

import (
	bdb "bitbucket.org/kisamoto/bumperdb"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

type UserResponse struct {
	UserID    string `json:"id"`
	FirstName string `json:"first_name,omitempty"`
	LastName  string `json:"last_name,omitempty"`
	Age       uint8  `json:"age,omitempty"`
	Gender    string `json:"gender,omitempty"`
	// URLs to 5 cropped profile pictures
	ProfilePictures [5]string `json:"pics,omitempty"`
}

type UserController struct {
	db *bdb.BumperDB
}

func (u *UserController) POST(c *gin.Context) {
	var errorType, errorDetail string
	var usrRequest NewUserRequest
	responseCode := http.StatusOK
	decoder := json.NewDecoder(c.Request.Body)

	err := decoder.Decode(&usrRequest)
	if err != nil {
		log.Println(err)
		errorType = errTypeInvalidJSON
		errorDetail = errDetailInvalidJSON
		responseCode = http.StatusBadRequest
	} else {
		log.Println("I'm fine")
	}
	c.JSON(responseCode, NewServerResponse(responseCode, errorType, errorDetail, nil))
}

func (u *UserController) PUT(c *gin.Context) {
	var errorType, errorDetail string
	var usrRequest UpdateUserRequest
	responseCode := http.StatusOK
	decoder := json.NewDecoder(c.Request.Body)
	userID := c.Params.ByName(userId)

	err := decoder.Decode(&usrRequest)
	if err != nil {
		log.Println(err)
		errorType = errTypeInvalidJSON
		errorDetail = errDetailInvalidJSON
		responseCode = http.StatusBadRequest
	} else {
		log.Printf("I'm fine for: %v", userID)
	}
	c.JSON(responseCode, NewServerResponse(responseCode, errorType, errorDetail, nil))
}

func (u *UserController) GET(c *gin.Context) {
	var errorType, errorDetail string
	var t UserResponse
	responseCode := http.StatusOK
	userID := c.Params.ByName(userId)

	lookupUserID := c.Request.URL.Query().Get(userId)
	if lookupUserID == "" {
		errorType = errTypeMissingParameter
		errorDetail = errDetailMissingParameter
		responseCode = http.StatusBadRequest
	} else {
		log.Printf("I'm fine for: %v to look up %v", userID, lookupUserID)
		t = UserResponse{UserID: userID}
	}
	c.JSON(responseCode, NewServerResponse(responseCode, errorType, errorDetail, t))
}

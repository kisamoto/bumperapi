package bumperapi

import (
	bdb "bitbucket.org/kisamoto/bumperdb"
	"time"
)

/*
Example POST data

{
	"locations": [{"lat": 1.1, "lon": 2.2, "datetimeRecorded": "2009-11-10T23:00:00Z"}]
}
The UserId/ownerIdentifier is extracted from the URL parameter
*/
type LocationRequest struct {
	Locations []bdb.Location `json:"locations"`
}

// ToDo: How to upload photos. Need to save to s3
type NewUserRequest struct {
	bdb.User
}

type UpdateUserRequest struct {
	bdb.User
}

type MessageRequest struct {
	FromUserId   string    `json:"from"`
	ToUserId     string    `json:"to"`
	SentDateTime time.Time `json:"sent"`
	Content      string    `json:"content"`
}

type BlockRequest struct {
	BlockingUserId string    `json:"blocking"`
	BlockDateTime  time.Time `json:"blockTime"`
}

type ReportRequest struct {
	ReportingUserId   string    `json:"reporting"`
	ReportingReason   string    `json:"reason"`
	ReportingDateTime time.Time `json:"reportTime"`
}

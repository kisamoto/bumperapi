package bumperapi

type HomepageRequest struct {
	Supported Supported `json:"supported"`
}

type Supported struct {
	Versions  []string `json:"versions"`
	Endpoints []string `json:"endpoints",omitempty`
}

func NewHomepage() *HomepageRequest {
	return &HomepageRequest{}
}

func (h *HomepageRequest) SetVersionsEndpoints(v []string, e []string) {
	h.Supported.SetVersions(v)
	h.Supported.SetEndpoints(e)
}

func (h *Supported) SetVersions(v []string) {
	h.Versions = v
}

func (h *Supported) AddVersion(v string) {
	h.Versions = append(h.Versions, v)
}

func (h *Supported) SetEndpoints(e []string) {
	h.Endpoints = e
}

func (h *Supported) AddEndpoint(e string) {
	h.Endpoints = append(h.Endpoints, e)
}

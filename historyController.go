package bumperapi

import (
	bdb "bitbucket.org/kisamoto/bumperdb"
	"github.com/gin-gonic/gin"
)

type HistoryController struct {
	db *bdb.BumperDB
}

func (h *HistoryController) GET(c *gin.Context) {
	user := c.Params.ByName(userId)
	// Just echo back for now
	c.String(200, user)
}

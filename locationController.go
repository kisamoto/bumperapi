package bumperapi

import (
	bdb "bitbucket.org/kisamoto/bumperdb"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"strconv"
)

type LocationController struct {
	db *bdb.BumperDB
}

func (l *LocationController) POST(c *gin.Context) {
	var errorType, errorDetail string
	var locRequest LocationRequest
	responseCode := http.StatusOK
	decoder := json.NewDecoder(c.Request.Body)
	userID, err := strconv.ParseInt(c.Params.ByName(userId), 10, 64)
	if err != nil {
		log.Println(err)
		errorType = errTypeInvalidParameter
		errorDetail = errDetailInvalidParameter
		responseCode = http.StatusBadRequest
		c.JSON(responseCode, NewServerResponse(responseCode, errorType, errorDetail, nil))
		return
	}
	err = decoder.Decode(&locRequest)
	if err != nil {
		log.Println(err)
		errorType = errTypeInvalidJSON
		errorDetail = errDetailInvalidJSON
		responseCode = http.StatusBadRequest
	} else {
		go l.QueueLocations(userID, locRequest.Locations)
	}
	c.JSON(responseCode, NewServerResponse(responseCode, errorType, errorDetail, nil))
}

func (l *LocationController) QueueLocations(userId int64, locs []bdb.Location) {
	for _, loc := range locs {
		lp := &loc
		lp.UserId = userId
		l.db.LocPush(loc)
	}
}

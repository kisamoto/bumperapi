package bumperapi

import (
	bdb "bitbucket.org/kisamoto/bumperdb"
	"github.com/gin-gonic/gin"
)

type BumperAPI struct {
	db *bdb.BumperDB
}

func NewServer(
	redisProtocol, redisHost, redisLocQueue, redisMsgQueue,
	postgresURI, postgresUserSchema, postgresAnalyticsSchema string) (api *BumperAPI) {

	db := bdb.NewBumperDB()
	db.SetRedis(redisProtocol, redisHost, redisLocQueue, redisMsgQueue)
	db.SetupPostgres(postgresURI, postgresUserSchema, postgresAnalyticsSchema)
	api = &BumperAPI{db: db}
	return
}

func (b *BumperAPI) Run(host string) {
	r := gin.Default()

	// Controller Setup
	hc := HomeController{}
	l := LocationController{db: b.db}
	u := UserController{db: b.db}
	bc := BumpController{db: b.db}
	le := LikeController{db: b.db}
	m := MessageController{db: b.db}
	hi := HistoryController{db: b.db}
	bl := BlockController{db: b.db}
	rp := ReportController{db: b.db}

	// Routes Setup
	r.GET(home, hc.GET)
	v1 := r.Group(version1)
	v1Loc := v1.Group(locGroup)
	{
		v1Loc.POST("/:"+userId, l.POST)
	}
	v1User := v1.Group(userGroup)
	{
		v1User.POST(home, u.POST)
		v1User.PUT("/:"+userId, u.PUT)
		v1User.GET("/:"+userId, u.GET)
	}
	v1Bump := v1.Group(bumpGroup)
	{
		v1Bump.GET("/:"+userId, bc.GET)
	}
	v1Like := v1.Group(likeGroup)
	{
		v1Like.POST("/:"+userId, le.POST)
	}
	v1Message := v1.Group(messageGroup)
	{
		v1Message.POST("/:"+userId, m.POST)
		v1Message.GET("/:"+userId, m.GET)
	}
	v1History := v1.Group(historyGroup)
	{
		v1History.GET("/:"+userId, hi.GET)
	}
	v1Block := v1.Group(blockGroup)
	{
		v1Block.POST("/:"+userId, bl.POST)
	}
	v1Report := v1.Group(reportGroup)
	{
		v1Report.POST("/:"+userId, rp.POST)
	}

	// Run
	r.Run(host)
}

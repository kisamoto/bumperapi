package bumperapi

import (
	bdb "bitbucket.org/kisamoto/bumperdb"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

type BlockController struct {
	db *bdb.BumperDB
}

func (m *BlockController) POST(c *gin.Context) {
	var errorType, errorDetail string
	var blkRequest BlockRequest
	responseCode := http.StatusOK
	decoder := json.NewDecoder(c.Request.Body)
	userID := c.Params.ByName(userId)
	err := decoder.Decode(&blkRequest)
	if err != nil {
		log.Println(err)
		errorType = errTypeInvalidJSON
		errorDetail = errDetailInvalidJSON
		responseCode = http.StatusBadRequest
	} else {
		log.Printf("I'm fine for: %v", userID)
	}
	c.JSON(responseCode, NewServerResponse(responseCode, errorType, errorDetail, nil))
}

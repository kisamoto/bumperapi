package bumperapi

import (
	"github.com/gin-gonic/gin"
)

type HomeController struct {
}

func (h *HomeController) GET(c *gin.Context) {
	var errorType, errorDetail string
	hp := NewHomepage()
	hp.SetVersionsEndpoints(
		[]string{version1},
		[]string{locGroup, userGroup, bumpGroup, likeGroup, messageGroup, historyGroup})
	res := NewServerResponse(200, errorType, errorDetail, hp)
	c.JSON(200, res)
}
